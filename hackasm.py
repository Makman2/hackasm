#!/usr/bin/env python3

import re
import sys
from argparse import ArgumentParser
from itertools import zip_longest
from pathlib import Path
from typing import NamedTuple, Union


parser = ArgumentParser()
parser.add_argument('file')
parser.add_argument('--out', '-o', default=None)
parser.add_argument('--bin', '-b', action='store_true')


class SyntaxError(Exception):
    def __init__(self, msg, lineno=None, file=None, command=None):
        super().__init__(msg)
        self.lineno = lineno
        self.file = file
        self.command = command


def strip_comments(asm: str):
    return re.sub(r'//.*', '', asm)


def strip_whitespace(string: str):
    return re.sub(r'[ \t]+', '', string)


class Pass1AInstruction(NamedTuple):
    token: Union[str, int]


class AInstruction(NamedTuple):
    value: int

    def assemble(self):
        binary = bin(self.value)[2:]
        return '0' * (16 - len(binary)) + binary


class CInstruction(NamedTuple):
    dest_op: str
    comp_op: str
    jump_op: str

    JUMPS = {
        None: '000',
        'JGT': '001',
        'JEQ': '010',
        'JGE': '011',
        'JLT': '100',
        'JNE': '101',
        'JLE': '110',
        'JMP': '111',
    }
    REGISTERS = {
        'A',
        'M',
        'D',
    }
    COMP_OPERANDS = {
        '0': '0101010',
        '1': '0111111',
        '-1': '0111010',
        'D': '0001100',
        'A': '0110000',
        '!D': '0001101',
        '!A': '0110001',
        '-D': '0001111',
        '-A': '0110011',
        'D+1': '0011111',
        '1+D': '0011111',
        'A+1': '0110111',
        '1+A': '0110111',
        'D-1': '0001110',
        'A-1': '0110010',
        'D+A': '0000010',
        'A+D': '0000010',
        'D-A': '0010011',
        'A-D': '0000111',
        'D&A': '0000000',
        'A&D': '0000000',
        'D|A': '0010101',
        'A|D': '0010101',
        'M': '1110000',
        '!M': '1110001',
        '-M': '1110011',
        'M+1': '1110111',
        '1+M': '1110111',
        'M-1': '1110010',
        'D+M': '1000010',
        'M+D': '1000010',
        'D-M': '1010011',
        'M-D': '1000111',
        'D&M': '1000000',
        'M&D': '1000000',
        'D|M': '1010101',
        'M|D': '1010101',
    }

    def assemble(self):
        sops = set(self.dest_op)
        return (
            '111' +
            self.COMP_OPERANDS[self.comp_op] +
            ('1' if 'A' in sops else '0') +
            ('1' if 'D' in sops else '0') +
            ('1' if 'M' in sops else '0') +
            self.JUMPS[self.jump_op]
        )


def chunk(iterable, n, fillvalue=None):
    """
    Collect data into fixed-length chunks or blocks:

    chunk('ABCDEFG', 3, 'x') --> ABC DEF Gxx"

    https://docs.python.org/3.7/library/itertools.html#itertools-recipes
    """
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def main(args):
    with open(args.file) as fl:
        asm = fl.read()

    try:
        code = assemble(asm, args.file)
    except SyntaxError as ex:
        print(f'[{ex.file}] line {ex.lineno}: {str(ex)}\n\n{ex.command}\n')
        return 1

    outfile = (Path(args.file).stem + '.' + ('bin' if args.bin else 'hack')) if args.out is None else args.out

    if args.bin:
        data = bytes(int(''.join(instruction), 2) for instruction in chunk(code, 8))
        with open(outfile, 'wb') as fl:
            fl.write(data)
    else:
        data = ''.join(''.join(c) + '\n' for c in chunk(code, 16))
        with open(outfile, 'w') as fl:
            fl.write(data)


def assemble(asm: str, file: str=None):
    # General purpose labels. A few are predefined.
    labels = {
        'R0': 0,
        'R1': 1,
        'R2': 2,
        'R3': 3,
        'R4': 4,
        'R5': 5,
        'R6': 6,
        'R7': 7,
        'R8': 8,
        'R9': 9,
        'R10': 10,
        'R11': 11,
        'R12': 12,
        'R13': 13,
        'R14': 14,
        'R15': 15,
        'SCREEN': 16384,
        'KBD': 24576,
        'SP': 0,
        'LCL': 1,
        'ARG': 2,
        'THIS': 3,
        'THAT': 4,
    }
    # FIXME What is not accounted for in code right now is when we define so many labels this table gets full or
    #  reaches e.g. the SCREEN memory-mapped area.
    variable_counter = 16  # We start at R15+1 for variable labels.
    # Instructions sequence.
    instructions = []

    for line, codeline in enumerate(asm.splitlines(), start=1):
        # Preprocess code line.
        command = strip_whitespace(strip_comments(codeline))

        if not command:
            # Empty line
            continue

        # Match labels.
        if command.startswith('('):
            match = re.fullmatch(r'\((.*)\)', command)
            if not match:
                raise SyntaxError('Invalid jump label syntax.', line, file, codeline)

            token = match.group(1)

            if not token:
                raise SyntaxError('Missing jump token.', line, file, codeline)

            # Enforce strong token.
            if not re.fullmatch(r'[a-zA-Z_]\w*', token):
                raise SyntaxError(
                    f'Invalid jump token "{token}". Must match [a-zA-Z_][a-zA-Z0-9_]*.',
                    line, file, codeline)

            labels[token] = len(instructions)
        else:
            # If not a label, it's an instruction.
            if command.startswith('@'):
                # A-instruction.
                value = command[1:]
                if not value:
                    raise SyntaxError(
                        'No value given for A-instruction.',
                        line, file, codeline)

                if re.fullmatch(r'\d+', value):
                    value = int(value)
                    if value >= 2 ** 15:
                        raise SyntaxError(
                            f'A-instruction value "{value}" exceeds 2^15-1 (> 32767).',
                            line, file, codeline)
                elif re.fullmatch(r'[a-zA-Z_]\w*', value):
                    if value not in labels:
                        labels[value] = variable_counter
                        variable_counter += 1
                else:
                    raise SyntaxError(
                        f'Invalid token "{value}" for A-instruction. '
                        f'Must be either an integer or a label matching [a-zA-Z_][a-zA-Z0-9_]*.',
                        line, file, codeline)

                instructions.append(Pass1AInstruction(value))
            else:
                # C-instruction.
                match = re.fullmatch(r'(?:(\w+)=)?([a-zA-Z0-9_+\-!&|]+)(?:;(\w+))?', command)
                if not match:
                    raise SyntaxError('Invalid syntax', line, file, codeline)

                dest_op = match.group(1) if match.group(1) is not None else ''
                comp_op = match.group(2)
                jump_op = match.group(3)

                if not (dest_op and comp_op) and not (comp_op and jump_op):
                    raise SyntaxError(
                        f'Invalid syntax. Missing one/some of required dest, comp or jump operands: dest=comp;jump',
                        line, file, codeline)
                if comp_op not in CInstruction.COMP_OPERANDS:
                    raise SyntaxError(
                        f'Invalid compare register "{comp_op}". '
                        f'Available registers are: {", ".join(CInstruction.COMP_OPERANDS)}.',
                        line, file, codeline)
                if any(op not in CInstruction.REGISTERS for op in dest_op):
                    raise SyntaxError(
                        f'Invalid destination register found in "{dest_op}". '
                        f'Available registers are (alone or in combination): {", ".join(CInstruction.REGISTERS)}.',
                        line, file, codeline)
                if jump_op not in CInstruction.JUMPS:
                    raise SyntaxError(
                        f'Invalid jump instruction "{jump_op}". '
                        f'Available jumps are: {", ".join(j for j in CInstruction.JUMPS if j is not None)}',
                        line, file, codeline)

                instructions.append(CInstruction(dest_op, comp_op, jump_op))

    # Postprocess to sanitize labels and jump addresses.
    pass2instructions = []
    for instruction in instructions:
        if isinstance(instruction, Pass1AInstruction):
            if isinstance(instruction.token, int):
                val = instruction.token
            else:
                val = labels[instruction.token]
            pass2instructions.append(AInstruction(val))
        else:
            pass2instructions.append(instruction)

    return ''.join(instruction.assemble() for instruction in pass2instructions)


if __name__ == '__main__':
    sys.exit(main(parser.parse_args()))
