# hackasm

Hack-assembler for the Hack-machine from nand2tetris.org!

## Usage

Python 3 is required to run the assembler:

```bash
./hackasm.py code.asm
# or alternatively
python3 hackasm.py code.asm
```

For more options, please query `./hackasm.py --help`.
